module Bertof.UtilitiesSpec where

import Test.Hspec
import Test.QuickCheck

import Bertof.Utilities

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "filterByTuplePosition" $ do
    it "filters the list of tuples containing the position" $ do
          filterByTuplePosition 1 [(0,2), (0,1), (1,2), (2,3)] `shouldBe` [(0,2),(0,1),(1,2)]

  describe "mean" $ do
    it "calculates the mean of a list of numbers" $ do
      average [1,2,3] `shouldBe` 2
      average [(-5)..5] `shouldBe` 0


  describe "standard_error" $ do
    it "calculates the standard error of a list of numbers" $ do
      stdDev [1,2,3] `shouldBe` 0.8164966
      stdDev [1,1,1] `shouldBe` 0

  describe "addLists" $ do
      it "adds two lists element wise" $ do
        addLists [] [] `shouldBe` []
        addLists [0..100] [] `shouldBe` [0..100]
        addLists [] [0..100] `shouldBe` [0..100]
        addLists [0..50] [0..50] `shouldBe` [2*x|x <- [0..50]]
