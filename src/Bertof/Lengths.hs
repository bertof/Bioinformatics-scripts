module Bertof.Lengths where

import Bio.SamTools.Bam
import Data.List
import Data.Maybe

import Bertof.Utilities

avgLengths :: FilePath -> IO [Int]
avgLengths bamFile = do
  bams <- readBams bamFile
  let maxPos = maximum $ map snd $ mapMaybe toPositionTuple bams
      avgLengthReadsInPosition i = res
        where list = map fromIntegral $ mapMaybe toLength $ filterByReadPosition i bams
              res | sum list == 0 = 0
                  | otherwise = average list

  return [round $ avgLengthReadsInPosition i | i <- [0..maxPos]]
