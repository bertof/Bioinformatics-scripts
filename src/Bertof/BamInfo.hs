module Bertof.BamInfo (bamInfo) where

import Bio.SamTools.Bam
import GHC.Int (Int64)
import Data.Maybe
import Data.List
import System.IO

import Bertof.Utilities

bamInfo :: FilePath -> Float -> IO()
bamInfo bamFile n = do
  readsLS <- readBams bamFile
  let lengths = map fromIntegral $ mapMaybe toLength readsLS
  putStrLn $ intercalate "\n" [
    "FILE:\t" ++ bamFile,
    "READS",
    "\tTot:\t\t" ++ show (length lengths),
    "LENGTHS",
    "\tMax:\t\t" ++ show (maximum lengths),
    "\tMin:\t\t" ++ show (minimum lengths),
    "\tMean:\t\t" ++ show (average lengths),
    "\tStd. err:\t" ++ show (stdDev lengths)]
  putStrLn $ "\nKeeping reads in " ++ show n ++ " standard deviations"
  let filteredLengths = filter (\x ->  abs(x - average lengths) <= n * stdDev lengths) lengths
  putStrLn $ intercalate "\n" [
    "READS",
    "\tTot:\t\t" ++ show (length filteredLengths),
    "LENGHTS",
    "\tMax:\t\t" ++ show (maximum filteredLengths),
    "\tMin:\t\t" ++ show (minimum filteredLengths),
    "\tMean:\t\t" ++ show (average filteredLengths),
    "\tStd. err:\t" ++ show (stdDev filteredLengths)]
