module Bertof.Risk where

import Bio.SamTools.Bam
import Data.List
import Data.Maybe

import Bertof.Utilities

anomaly :: FilePath -> IO [Int]
anomaly bamFile = do
  bams <- readBams bamFile
  let insertLengths = mapMaybe toLength bams
      n = 2
      maxPos = maximum insertLengths
      riskyBams = filter (\x -> case toLength x of Just y -> abs(fromIntegral y - average (map fromIntegral insertLengths)) > n * stdDev (map fromIntegral insertLengths); _ -> False) bams

      readsInPosition i = fromIntegral.length $ filterByReadPosition i bams
      riskyReadsInPosition i = fromIntegral.length $ filterByReadPosition i riskyBams

  return [case riskyReadsInPosition i of 0 -> 0; _ -> round(riskyReadsInPosition i / readsInPosition i * 100) | i <- [0..maxPos]]