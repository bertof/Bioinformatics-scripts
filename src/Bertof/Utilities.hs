module Bertof.Utilities where

import Bio.SamTools.Bam
import GHC.Int
import Data.Maybe
import Control.Applicative

type PositionTuple = (Int64, Int64)

-- Extract Maybe length of a read
toLength :: Bam1 -> Maybe Int64
toLength = queryLength

-- Extracts the position of the begin and the end of a mate pair, combining it in a tuple
toPositionTuple :: Bam1 -> Maybe PositionTuple
toPositionTuple bam = do
  pos <- position bam
  len <- toLength bam
  Just (pos, len)

-- Filters a tuple list based on the position of the index i:
-- if i is inside the tuple, that one is kept, otherwise it's dropped
filterByTuplePosition :: Int64 -> [PositionTuple] -> [PositionTuple]
filterByTuplePosition pos = filter (\t -> fst t <= pos && pos <= snd t)

-- Filters a tuple list based on the position of the index i:
-- if i is inside the tuple, that one is kept, otherwise it's dropped
filterByReadPosition :: Int64 -> [Bam1] -> [Bam1]
filterByReadPosition pos = filter (\x -> case containsPosition pos x of Just True -> True; _ -> False)


-- Returns a Just Bool if the read contains the position passed
containsPosition :: Int64 -> Bam1 -> Maybe Bool
containsPosition pos bam = do
  pt <- toPositionTuple bam
  return (fst pt <= pos && pos <= snd pt)


-- Calculate mean of a list of numbers
average :: [Float] -> Float
average = (/) <$> sum <*> realToFrac . length

-- Calculate standard error of a list of numbers
stdDev :: [Float] -> Float
stdDev xs = sqrt . average . map ((^2) . (-) axs) $ xs
   where axs     = average xs

-- Sums two lists element wise
addLists :: Num a => [a] -> [a] -> [a]
addLists (x:xs) (y:ys) = (x+y): addLists xs ys
addLists [] ys = ys
addLists xs [] = xs