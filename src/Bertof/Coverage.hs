module Bertof.Coverage (
  coverage,
  coverage',
  toPositionTuple,
  addLists,
  filterByTuplePosition
) where

import Bio.SamTools.Bam
import GHC.Int (Int64)
import Data.Maybe
import System.IO

import Bertof.Utilities

-- Produces a list of ones in the position occupied by the tuple and zeros before.
-- Assumes the first element of the tuple is <= to the second one
tupleTo01List :: PositionTuple -> [Int]
tupleTo01List t = replicate (fromIntegral (fst t)) 0 ++ replicate (fromIntegral (snd t) - fromIntegral (fst t) + 1) 1

-- Calculates coverage for each position (version using filters)
coverage :: FilePath -> IO [Int]
coverage bamFile = do
  readsLS <- readBams bamFile
  let tuples = mapMaybe toPositionTuple readsLS
      maxPos = maximum $ map snd tuples
  return [length $ filterByTuplePosition i tuples  | i <- [0..maxPos]]

-- Calculates coverage for each position (version using addition over lists)
coverage' :: FilePath -> IO [Int]
coverage' indexedBamFile = do
  bams <- readBams indexedBamFile
  let tuples = map toPositionTuple bams
  return $ foldl1 addLists (fmap tupleTo01List $ catMaybes tuples)
