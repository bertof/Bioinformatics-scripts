module Bertof.Uniques where

import Bio.SamTools.Bam
import Data.List
import Data.Maybe

import Bertof.Utilities

uniques :: FilePath -> IO [Int]
uniques bamFile = do
  bams <- readBams bamFile
  let uniqueReads = filter (not . isDup) bams
      tuples = mapMaybe toPositionTuple uniqueReads
      maxPos = maximum $ map snd tuples
  return [length $ filterByTuplePosition i tuples  | i <- [0..maxPos]]