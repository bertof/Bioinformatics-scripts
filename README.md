# BioinfoProject
## Filippo Berto - AA 2017/2018 - University of Padua (Università degli Studi di Padova)

[![pipeline status](https://gitlab.com/bertof/Bioinformatics-scripts/badges/master/pipeline.svg)](https://gitlab.com/bertof/Bioinformatics-scripts/commits/master)


### Introduction

This document describes the process used to analyze a FASTA file, containing numerous experimental reads of a DNA sequence, and a complete genome. Since some operations are non trivial and need the high computational power of a computer, it's necessary to write some scripts to produce the results needed for the analysis; in this case, I choose to implement a small program in Haskell that reads a SAM file, containing the reads aligned by the tool `samtools`, and generates useful information via WIG tracks or standard output on the terminal. The main goal of the program is to be efficient, using a fixed amount of RAM, and fast, allowing to use all the cores of the computer running it.

### Prerequisites

To work with genomes and reads files we'll need some utility programs, in particular the following are the one used and assumed to be installed on the system:
- [BWA](http://bio-bwa.sourceforge.net/)
- [Samtools](http://www.htslib.org/)
- [IGV](http://software.broadinstitute.org/software/igv/home)

### Indicization and alignment

The first task of the project is to prepare the data given for further analysis.
In particular, starting from the genome of the Lactobacillus Casei and two FASTA read files, the approach is to create an index of the genome using bwa:

```shell
bwa index Lactobacillus_casei_genome.fasta
```

This command will produce the following files:

```shell
Lactobacillus_casei_genome.fasta.amb
Lactobacillus_casei_genome.fasta.ann
Lactobacillus_casei_genome.fasta.bwt
Lactobacillus_casei_genome.fasta.pac
Lactobacillus_casei_genome.fasta.sa
```

Now we want to align the reads to the genome; we'll use the following command:

```shell
bwa mem -t 8 Lactobacillus_casei_genome.fasta lact_sp.read1.fastq lact_sp.read2.fastq > lact.sam
```

This will generate a SAM file, containing all the information on the alignments in a readable format. The flag ```-t 8``` sets the number of threads available to the process.

Then, we want to convert the SAM file to a binary format, which is far more space efficient and easier to parse, generating a BAM file.

```shell
samtools view ­bS lact_sp.sam > lact.bam
```

Since the reads might not be sorted, it's a good idea to sort them, making easier to apply some specific algorithms later.

```shell
samtools sort lact.bam -o lact_sorted.bam
```

Finally, we create the index of the reads using:

```shell
samtools index lact_sorted.bam
```

The final result will be the two following files:

```shell
lact_sorted.bam
lact_sorted.bam.bai
```

### Analysis of the reads

We can use the open-source program IGV to view the content of the produced files.
A preliminary analysis of the reads doesn't show any particular anomaly, but after a more accurate inspection of the coverage I found three loci which have some irregularity.

1. Near 1400kb from the start we can see a large drop in coverage and a mismatch in the reads.

    ![ins1](Ins1.png)
   
    In this case it looks IGV reports that the mate paris have a significant mismatch, suggesting an insert is occurred. Since the gap is about 15kb long it looks like if the reads of that section are totally missing.
   
2. Near 1755kb from the start we find a similar incongruity.

    ![Ins2](Ins2.png)
   
    As before, the gap is about 15kb large and shows that some reads are missing.
   
3. In the proximity of 2449kb we can observe a smaller but significant drop in coverage.

    ![Ins3](Ins3.png)
   
    The gap is 1kb large and less uniform compared to the other two.
   
### Automation by scripting
The program is automatically compiled and built for Linux x64 on each new release on this repository and can be download at the following [link](https://gitlab.com/bertof/Bioinformatics-scripts/-/jobs/artifacts/master/download?job=release). Run the program without any argument, or passing the argument `--help` to have a list of the analysis this too implements.

You can use the argument `info` to get some data about the bam file:
```shell
$ BioinfoProject info data/lact_sorted.bam
FILE:   data/lact_sorted.bam
READS
        Tot:            1224683
LENGTHS
        Max:            2547685.0
        Min:            445.0
        Mean:           16681.676
        Std. err:       149308.11

Keeping reads in 2.0 standard deviations
READS
        Tot:            1211599
LENGHTS
        Max:            315294.0
        Min:            445.0
        Mean:           2783.3574
        Std. err:       10990.8125
```

The other arguments, like `coverage` and `lengths` can output the results on the terminal or on a WIG file, using the option `-o <name of the wig file>` before the path to the bam file:

```shell
BioinfoProject coverage -o test.wig data/lact_sorted.bam
```

This will create the file test.wig with the following content:

```wig
fixedStep chrom=genome start=1 step=11	0
2	0
3	0
4	2
5	2
6	2
7	2
8	3
9	4
10	5
...
```

The wig file can then be imported in a visualizer, like IGV. 

### Build notes
If you're building this in Ubuntu 16.04 you should install the package `c2hs` with the following command:
```bash
sudo apt install c2hs
```
This is necessary since I need to handle low level variables (GHC.Int Int64) coming from the [samtools](http://hackage.haskell.org/package/samtools-0.2.4.3) binding library.