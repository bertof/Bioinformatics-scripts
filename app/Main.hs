module Main where

import System.Environment
import System.IO
import Bertof.Coverage
import Bertof.BamInfo
import Bertof.Risk
import Bertof.Uniques
import Bertof.Lengths

import Data.List

import qualified Data.ByteString.Lazy.Char8 as BS8

main :: IO ()
main = do
  let n = 2
  args <- getArgs
  case length args of
    0 -> printHelp n
    _ ->
      case head args of
        "help" -> printHelp n

        _ -> do
          let bamFile = last (tail args)
              maybeOutputFileArgIndex = "-o" `elemIndex` args

          case head args of
            "info" -> do
              bamInfo bamFile n

            "coverage" -> do
              res <- coverage bamFile
              let output = BS8.unlines . map BS8.pack $ [show (fst t) ++ "\t" ++ show (snd t) | t <- zip [1..] res]

              case maybeOutputFileArgIndex of
                Nothing -> do
                  BS8.putStrLn $ BS8.pack "Pos.\tCount"
                  BS8.putStrLn output

                _ -> do
                  let Just ind = maybeOutputFileArgIndex
                      outputFile = (args !! (ind+1))
                  BS8.writeFile outputFile (BS8.pack "fixedStep chrom=genome start=1 step=1")
                  BS8.appendFile outputFile output

            "anomaly" -> do
              res <- anomaly bamFile
              let output = BS8.unlines . map BS8.pack $ [show (fst t) ++ "\t" ++ show (snd t) | t <- zip [1..] res]

              case maybeOutputFileArgIndex of
                Nothing -> do
                  BS8.putStrLn $ BS8.pack "Pos.\tCount"
                  BS8.putStrLn output

                _ -> do
                  let Just ind = maybeOutputFileArgIndex
                      outputFile = (args !! (ind+1))
                  BS8.writeFile outputFile (BS8.pack "fixedStep chrom=genome start=1 step=1")
                  BS8.appendFile outputFile output

            "uniques" -> do
              res <- uniques bamFile
              let output = BS8.unlines . map BS8.pack $ [show (fst t) ++ "\t" ++ show (snd t) | t <- zip [1..] res]

              case maybeOutputFileArgIndex of
                Nothing -> do
                  BS8.putStrLn $ BS8.pack "Pos.\tCount"
                  BS8.putStrLn output

                _ -> do
                  let Just ind = maybeOutputFileArgIndex
                      outputFile = (args !! (ind+1))
                  BS8.writeFile outputFile (BS8.pack "fixedStep chrom=genome start=1 step=1")
                  BS8.appendFile outputFile output

            "lengths" -> do
              res <- avgLengths bamFile
              let output = BS8.unlines . map BS8.pack $ [show (fst t) ++ "\t" ++ show (snd t) | t <- zip [1..] res]

              case maybeOutputFileArgIndex of
                Nothing -> do
                  BS8.putStrLn $ BS8.pack "Pos.\tAvg. len"
                  BS8.putStrLn output

                _ -> do
                  let Just ind = maybeOutputFileArgIndex
                      outputFile = (args !! (ind+1))
                  BS8.writeFile outputFile (BS8.pack "fixedStep chrom=genome start=1 step=1")
                  BS8.appendFile outputFile output

            _ -> printHelp n




printHelp :: Float -> IO ()
printHelp n = sequence_ [putStrLn x | x <- [
  "A simple Haskell script for coverage analysis by Filippo Berto (bertof)",
  "USAGE",
  "\tBioinfoProject <command> [options] <bam file>",
  "COMMANDS",
  "\tcoverage\tCalculates the coverage",
  "\t\t-o <wigfile>\tWrites the result to a wig file",
  "\tanomaly\tCalculates the coverage of the reads that have length above or below " ++ show n ++" times the standard deviance",
  "\t\t-o <wigfile>\tWrites the result to a wig file",
  "\tuniques\tCalculates the coverage of the reads that are uniques",
  "\t\t-o <wigfile>\tWrites the result to a wig file",
  "\tlengths\tCalculates the the rounded average length of the reads in each position",
  "\t\t-o <wigfile>\tWrites the result to a wig file",
  "\tinfo\tDisplays informations about the file",
  "\thelp\tDisplay this help page"
  ]]

